﻿/*
* Сачков Степан, БПИ192
* УСЛОВИЕ: Задача о производстве булавок. В цехе по заточке булавок все
* необходимые операции осуществляются тремя рабочими. Первый из них
* берет булавку и проверяет ее на предмет кривизны. Если булавка не кривая,
* то рабочий передает ее своему напарнику. Напарник осуществляет
* собственно заточку и передает заточенную булавку третьему рабочему,
* который осуществляет контроль качества операции. Требуется создать
* многопоточное приложение, моделирующее работу цеха. При решении
* использовать парадигму «производитель-потребитель».
*/
#include <iostream>
#include <optional>
#include <mutex>
#include <random>

#include <omp.h>


/// <summary>
/// Структура, описывающая булавку.
/// </summary>
struct Pin {
	long const pinNumber; // номер булавки

	explicit Pin(long n) : pinNumber(n) {}
};

std::mutex mtx; // мутекс
long numberOfPins = 0; // количество булавок

// Объекты, которые будут хранить булавки.
// ВНИМАНИЕ: optional доступен только с C++17.
std::optional<Pin> currentObj, countedObj;

/// <summary>
/// Процесс проверки булавок на кривизну.
/// </summary>
/// <param name="count">Количество булавок</param>
void checkCurvature(long count) {
	while (count > 0) {
		if (!currentObj.has_value()) {
			// Защита от владения двумя объектами сразу.
			std::lock_guard<std::mutex> lock(mtx);

			std::cout << "The first worker checks if the pin_" << numberOfPins - count + 1 << " is curved . . ." << std::endl;

			if (rand() % 2 == 0) {
				std::cout << "The pin is NOT curvated! The first worker passes the pin_" <<
					numberOfPins - count + 1 << " to his partner\n" << std::endl;

				currentObj.emplace(numberOfPins - count + 1);
			}
			else {
				std::cout << "The pin_" <<
					numberOfPins - count + 1 << " turned out to be curvated!" << std::endl;

				currentObj.emplace(-1);
			}

			count--;
		}
	}
}

/// <summary>
/// Процесс заострения булавок.
/// </summary>
/// <param name="count">Количество булавок</param>
void sharpenPins(long count) {
	while (count > 0) {
		if (currentObj.has_value() && !countedObj.has_value()) {
			// Защита от владения двумя объектами сразу.
			std::lock_guard<std::mutex> lock(mtx);

			countedObj.emplace(currentObj.value());

			if (countedObj.value().pinNumber != -1) {
				std::cout << "The second worker sharpen the pin_" << currentObj.value().pinNumber << " . . .\n" << std::endl;
			}

			currentObj.reset();

			count--;
		}
	}
}

/// <summary>
/// Процесс контроля качества.
/// </summary>
/// <param name="count">Количество булавок</param>
void controlQuality(long count) {
	while (count > 0) {
		if (countedObj.has_value()) {
			// Защита от владения двумя объектами сразу.
			std::lock_guard<std::mutex> lock(mtx);

			if (countedObj.value().pinNumber != -1) {
				std::cout << "The third worker controls the quality of the pin_" <<
					countedObj.value().pinNumber << " . . ." << std::endl;

				if (rand() % 2 == 0) {
					std::cout << "The pin_" <<
						countedObj.value().pinNumber << " has a GOOD quality!\n" << std::endl;
				}
				else {
					std::cout << "The pin_" <<
						countedObj.value().pinNumber << " has a BAD quality!\n" << std::endl;
				}
			}

			countedObj.reset();

			count--;
		}
	}
}

int main() {
	std::cout << "Enter the number of pins: ";
	std::cin >> numberOfPins;

#pragma omp parallel
	{
		if (omp_get_thread_num() == 0) {
			checkCurvature(numberOfPins);
		}
		if (omp_get_thread_num() == 1) {
			sharpenPins(numberOfPins);
		}
		if (omp_get_thread_num() == 2) {
			controlQuality(numberOfPins);
		}
	}

	std::cout << "The work is over!" << std::endl;

	return 0;
}
